FROM alpine

ENV NAME=play-watche-test
ADD ./build /build
ADD ./entrypoint /entrypoint
ADD ./salt /salt

RUN apk --no-cache add bash

RUN bash < /entrypoint

